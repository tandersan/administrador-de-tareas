// Requires Paquetes
const fs = require('fs');

// Variables
let directorioBase = 'data';
let nombreBase = 'data.json';
let rutaBase = `./${directorioBase}/${nombreBase}`;
let listaTareasGuardadas = '';

const cargarDB = ( () => {
    try {
        listaTareasGuardadas = JSON.parse(fs.readFileSync(rutaBase, {encoding: 'utf-8'}));
    } catch (err) {
        console.log(err);
    }

    return listaTareasGuardadas;
});

const guardarDB = ( (data) => {
    return new Promise ( (resolve, reject) => {
        fs.writeFile(rutaBase, JSON.stringify(data), (err) => {
            if (err)
                reject(err)
            else
                resolve(nombreBase);
        });
    });
});

module.exports = {
    guardarDB,
    cargarDB
}