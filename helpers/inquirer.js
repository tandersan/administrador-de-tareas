// Requires Paquetes
require('colors');

//Variables
const inquirer = require('inquirer');

//MAIN
const inquirerMenu = async() => {
    const opcionesMenu = [
        {
            type: 'list',
            name: 'opcionSel',
            message: '¿Qué desea ejecutar? ',
            choices: [
                {
                    value: '1',
                    name: `${'1.'.green} Crear Tarea`
                },
                {
                    value: '2',
                    name: `${'2.'.green} Listar Todas las Tareas`
                },
                {
                    value: '3',
                    name: `${'3.'.green} Listar Tareas Completadas`
                },
                {
                    value: '4',
                    name: `${'4.'.green} Listar Tareas Pendientes`
                },
                {
                    value: '5',
                    name: `${'5.'.green} Completar Tarea(s)`
                },
                {
                    value: '6',
                    name: `${'6.'.green} Borrar Tarea`
                },
                {
                    value: '7',
                    name: `${'7.'.green} Salir\n`
                }
            ]
        }
    ]

    console.clear();
    console.log('========================='.green);
    console.log(' Menú gestión de Tareas: ');
    console.log('========================='.green);
    const { opcionSel } = await inquirer.prompt(opcionesMenu);
    return opcionSel;
}

const pausa = async() => {
    const opcionPausa = [
        {
            type: 'input',
            name: 'opcionPausa',
            message: `Presione ${'ENTER'.blue} para continuar ...`
        }
    ];

    await inquirer.prompt(opcionPausa);
}

const inputUsuario = async(message) => {
    const opcionInputUsuario = [
        {
            type: 'input',
            name: 'leeInputUsuario',
            message,
            validate(value) {
                if (value.length === 0) {
                    return `Favor inrgese un valor`.red
                } else {
                    return true;
                }
            }
        }
    ];

    const { leeInputUsuario } = await inquirer.prompt(opcionInputUsuario);
    return leeInputUsuario;
}

const listaTareasBorrar = async(tareas = []) => {
    let idFinalArregloTareas = tareas.length - 1;
    const choices = tareas.map( (tarea, idx) => {
        if (idFinalArregloTareas !== idx) {
            return {
                value: tarea.id,
                name: `${(++idx + '.').blue} [${tarea.descripcion}]`
            }
        } else {
            return {
                value: tarea.id,
                name: `${(++idx + '.').blue} [${tarea.descripcion}]\n`
            }
        }
    });

    choices.unshift({
        value: '0',
        name: `${'0.'.blue} ${'Salir sin Borrar'.green}`
    });

    const opcionTareasBorrarSel = [
        {
            type: 'list',
            name: 'idTareaABorrar',
            message: 'Seleccione la tarea a Borrar: ',
            choices
        }
    ];

    const { idTareaABorrar } = await inquirer.prompt(opcionTareasBorrarSel);
    return idTareaABorrar;
}

const listaTareasCompletar = async(tareas = []) => {
    const choices = tareas.map( (tarea, idx) => {
        return {
            value: tarea.id,
            name: `${(++idx + '.').blue} [${tarea.descripcion}]`,
            checked: (tarea.completado) ? true : false
        }
    });

    const opcionTareasCompletarSel = [
        {
            type: 'checkbox',
            name: 'idTareasACompletar',
            message: 'Seleccione la(s) tarea(s) a Completar: ',
            choices
        }
    ];

    const { idTareasACompletar } = await inquirer.prompt(opcionTareasCompletarSel);
    return idTareasACompletar;
}

const confirmar = async(mensaje) => {
    const confirmarOpcion = [
        {
            type: 'confirm',
            name: 'confirmado',
            message: mensaje
        }
    ];

    const { confirmado } = await inquirer.prompt(confirmarOpcion);
    return confirmado;
}

module.exports = {
    inquirerMenu,
    pausa,
    inputUsuario,
    listaTareasBorrar,
    listaTareasCompletar,
    confirmar
}