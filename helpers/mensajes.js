// Requires Paquetes
require('colors');

// Variables

// MAIN
const mostrarMenu = () => {
    return new Promise( (resolve, reject) => {
        console.clear();
        console.log('========================='.green);
        console.log(' Menú gestión de Tareas: '.green);
        console.log('========================='.green);
        console.log(`${'1.'.green} Crear Tarea`);
        console.log(`${'2.'.green} Listar Todas las Tareas`);
        console.log(`${'3.'.green} Listar Tareas Completadas`);
        console.log(`${'4.'.green} Listar Tareas Pendientes`);
        console.log(`${'5.'.green} Completar Tarea(s)`);
        console.log(`${'6.'.green} Borrar Tarea`);
        console.log(`${'0.'.green} Salir \n`);
    
        const readLine = require('readline').createInterface({
            input: process.stdin,
            output: process.stdout
        })
    
        readLine.question('Seleccione una Opción: ', (opcion) => {
            readLine.close();
            resolve(opcion);
        });
    })
}

const pausa = () => {
    return new Promise( (resolve, reject) => {
        const readLine = require('readline').createInterface({
            input: process.stdin,
            output: process.stdout
        })
    
        readLine.question(`Presione ${'ENTER'.blue} para continuar ...\n`, () => {
            readLine.close();
            resolve();
        });  
    })
}

module.exports = {
    mostrarMenu,
    pausa
}