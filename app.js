// Requires Paquetes
require('colors');

/*// Para utilizar con mensajes.js
//-------------------------------------------------------------------------------
//Variables
const { mostrarMenu, pausa } = require('./helpers/mensajes');

//MAIN
const main = async() => {
    let opcion = '';

    do {
        opcion = await mostrarMenu();
        if (opcion !== '0') await pausa();
    } while (opcion !== '0')
}*/
//-------------------------------------------------------------------------------


// Para utilizar con inquirer.js
//-------------------------------------------------------------------------------
//Variables
const { inquirerMenu, pausa, inputUsuario, listaTareasBorrar, listaTareasCompletar, confirmar } = require('./helpers/inquirer');
const Tareas = require('./models/tareas');
const { guardarDB, cargarDB } = require('./helpers/database');

//MAIN
const main = async() => {
    let opcion = '';
    const tareas = new Tareas();
    tareas.cargarTareas(cargarDB());

    do {
        opcion = await inquirerMenu();

        switch(opcion) {
            case '1':
                const descripcionTarea = await inputUsuario('Ingrese la descripcion de la Tarea: ');
                tareas.crearTarea(descripcionTarea);

                await guardarDB(tareas._listaTareasArr)
                    .then ( nombreArchivo => console.log(`Tarea guardada en base ${nombreArchivo}`.green + '\n') )
                    .catch ( err => + console.log(`\n\n${'[ERROR]'.red} Error en almacenamiento de tarea.`) + console.log(err) + console.log());
                break;

            case '2':
                tareas.formatearYListarTareas();
                break;

            case '3':
                tareas.formatearYListarTareas(2);
                break;

            case '4':
                tareas.formatearYListarTareas(3);
                break;

            case '5':
                const idTareasACompletar = await listaTareasCompletar(tareas._listaTareasArr);
                tareas.completarTarea(idTareasACompletar);

                await guardarDB(tareas._listaTareasArr)
                    .then ( console.log('La(s) tarea(s) ha(n) sido correctamente actualizada(s)\n'.green) )
                    .catch ( err => + console.log(`\n\n${'[ERROR]'.red} Error en actualización de tarea(s).`) + console.log(err) + console.log());
                break;

            case '6':
                const idTareaABorrar = await listaTareasBorrar(tareas._listaTareasArr);
                if (idTareaABorrar !== '0') {
                    const confirmado = await confirmar('¿Está seguro de borrar la tarea seleccionada?');
                    if (confirmado) {
                        idTareaQueBorraremos = tareas._listaTareasArr.findIndex(x => x.id === idTareaABorrar)
                        descTareaQueBorraremos = tareas._listaTareasArr[idTareaQueBorraremos].descripcion;
                        tareas.borrarTarea(idTareaABorrar);

                        await guardarDB(tareas._listaTareasArr)
                            .then ( console.log(`${'La tarea'.green} ${('[' + descTareaQueBorraremos + ']').yellow} ${'ha sido eliminada'.green}\n`) )
                            .catch ( err => + console.log(`\n\n${'[ERROR]'.red} Error en eliminado de tarea.`) + console.log(err) + console.log());
                    }
                }
                break;

            default:
                console.log('Doh!!!'.red);
                break;
        }

        if (opcion !== '7') await pausa();
    } while (opcion !== '7')
}
//-------------------------------------------------------------------------------

main();