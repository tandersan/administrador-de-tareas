// Variables
const { v4: uuidv4 } = require('uuid');

//MAIN
class Tarea {
    id = '';
    descripcion = '';
    completado = false;
    completadoEn = null

    constructor( descripcion ) {
        this.id = uuidv4();
        this.descripcion = descripcion;
        this.completado = false;
        this.completadoEn = null
    }
}

module.exports = Tarea;