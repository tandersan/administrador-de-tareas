//Variables
const Tarea = require('./tarea');

//MAIN
class Tareas {
    _listaTareas = {};

    get _listaTareasArr() {
        const arregloTmp = [];

        Object.keys(this._listaTareas).forEach( key => {
            arregloTmp.push(this._listaTareas[key]);
        })

        return arregloTmp;
    }

    constructor() {
        this._listaTareas = {};
    }

    crearTarea(descripcion = 'Sin Descripcion') {
        const tarea = new Tarea(descripcion);

        this._listaTareas[tarea.id] = tarea;
    }

    cargarTareas(listaTareasDeBase = []) {
        listaTareasDeBase.forEach( tarea => {
            this._listaTareas[tarea.id] = tarea;
        })
    }

    formatearYListarTareas(tipoLista = 1) {
        switch(tipoLista) {
            case 1:
                var tareaYaFormateada = '';
                var idxTareas = 1;

                this._listaTareasArr.forEach( tarea => {
                    let { descripcion, completado, completadoEn } = tarea;
                    let descripcionColor = `${descripcion}`;
                    let completadoEnColor = `${completadoEn}`.brightGreen;
        
                    if (completado) {
                        tareaYaFormateada += `${(idxTareas++ + '.').blue} [${descripcionColor}] => ${'Completada'.brightGreen} en fecha ${completadoEnColor}\n`;
                    } else {
                        tareaYaFormateada += `${(idxTareas++ + '.').blue} [${descripcionColor}] => ${'Pendiente'.yellow}\n`;
                    }
                })

                console.log(tareaYaFormateada);
                break;

            case 2:
                var tareaYaFormateada = '';
                var idxTareas = 1;

                this._listaTareasArr.forEach( tarea => {
                    let { descripcion, completado, completadoEn } = tarea;
                    let descripcionColor = `${descripcion}`;
                    let completadoEnColor = `${completadoEn}`.brightGreen;

                    if (completado) {
                        tareaYaFormateada += `${(idxTareas++ + '.').blue} [${descripcionColor}] => ${'Completada'.brightGreen} en fecha ${completadoEnColor}\n`;
                    }
                })

                console.log(tareaYaFormateada);
                break;

            case 3:
                var tareaYaFormateada = '';
                var idxTareas = 1;

                this._listaTareasArr.forEach( tarea => {
                    let { descripcion, completado } = tarea;
                    let descripcionColor = `${descripcion}`;
                    
                    if (!completado) {
                        tareaYaFormateada += `${(idxTareas++ + '.').blue} [${descripcionColor}] => ${'Pendiente'.yellow}\n`;
                    }
                })

                console.log(tareaYaFormateada);
                break;
        }
    }

    borrarTarea (id = '') {
        if(this._listaTareas[id]) {
            delete this._listaTareas[id];
        }
    }

    completarTarea (ids = []) {
        ids.forEach( id => {
            const tarea = this._listaTareas[id];
            if (!tarea.completado) {
                tarea.completado = true;
                tarea.completadoEn = new Date().toISOString();
            }
        })

        this._listaTareasArr.forEach( tarea => {
            if (!ids.includes(tarea.id)) {
                tarea.completado = false;
                tarea.completadoEn = "";
            }
        })

    }
}

module.exports = Tareas;